from distutils.command.upload import upload
from email.mime import image
from email.policy import default
from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    image = models.ImageField(default='defoult.jpg', upload_to = 'profile_pics')


    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 and img.width > 300:
            outpu_size = (300,300)
            img.thumbnail(outpu_size)
            img.save(self.image.path)

    def __str__(self):
        return f'Profile of {self.user.username}'
